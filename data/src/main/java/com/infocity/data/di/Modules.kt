package com.infocity.data.di

import com.infocity.data.dataservice.apiservice.InfoCityApiService
import com.infocity.data.dataservice.apiservice.OauthApiService
import com.infocity.data.dataservice.preferencesservice.AppSettingPreferences
import com.infocity.data.datastore.api.OauthRepository
import com.infocity.data.datastore.api.ServiceObjectTypesRepository
import com.infocity.data.datastore.preference.AppSettingRepository
import com.infocity.data.datastore.sql.ServiceObjectTypeRepository
import com.infocity.data.datastore.sql.UserRepository
import com.infocity.data.header.InfoCityHeaderInterceptor
import com.infocity.data.header.OauthHeaderInterceptor
import com.infocity.data.repository.api.OauthRepositoryImpl
import com.infocity.data.repository.api.ServiceObjectTypesRepositoryImpl
import com.infocity.data.repository.sql.ServiceObjectTypeRepositoryImpl
import com.infocity.data.repository.sql.UserRepositoryImpl
import com.infocity.data.utils.BASE_URL
import com.infocity.data.utils.CONNECTION_TIMEOUTS_MIN
import com.squareup.moshi.Moshi
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

val databaseModule = module {

    single<ServiceObjectTypeRepository> { ServiceObjectTypeRepositoryImpl() }
    single<UserRepository> { UserRepositoryImpl() }


}

    val apiModule = module {
        single { Moshi.Builder().build()}

        fun getRetrofit(baseUrl: String, interceptor: Interceptor, factory : Converter.Factory?): Retrofit {

            val retrofitBuilder = Retrofit.Builder().baseUrl(baseUrl)

            factory?.let {
                retrofitBuilder.addConverterFactory(factory)
            }
            return retrofitBuilder.apply {
                client(
                    OkHttpClient.Builder()
                        .retryOnConnectionFailure(true)
                        .addInterceptor(interceptor)
                        .addInterceptor(HttpLoggingInterceptor().apply {
                            level = HttpLoggingInterceptor.Level.BODY
                        })

                        .connectTimeout(CONNECTION_TIMEOUTS_MIN, TimeUnit.SECONDS)
                        .readTimeout(CONNECTION_TIMEOUTS_MIN, TimeUnit.SECONDS)
                        .writeTimeout(CONNECTION_TIMEOUTS_MIN, TimeUnit.SECONDS)
                        .build()
                )
            }
                .build()
        }


        single<OauthApiService> {
            getRetrofit(BASE_URL, OauthHeaderInterceptor(), MoshiConverterFactory.create().asLenient())
                .create(OauthApiService::class.java)
        }

        single<InfoCityApiService> {
            getRetrofit(BASE_URL, InfoCityHeaderInterceptor(get()), MoshiConverterFactory.create().asLenient())
                .create(InfoCityApiService::class.java)
        }

}

val preferencesModule = module {
    single<AppSettingRepository> {
        AppSettingPreferences(
            get()
        )
    }
}

val repositoryModule = module {
    single<OauthRepository> { OauthRepositoryImpl(get(), get(), get()) }
    single<ServiceObjectTypesRepository> { ServiceObjectTypesRepositoryImpl(get(), get(), get()) }

}


