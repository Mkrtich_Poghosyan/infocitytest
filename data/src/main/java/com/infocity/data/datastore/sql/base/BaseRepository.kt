package com.infocity.data.datastore.sql.base


interface BaseRepository<T> {

   suspend fun insert(item : T)

   suspend fun insertAll(itemList : List<T>)

   suspend fun getAll() : List<T>

   suspend fun deleteAll()
}