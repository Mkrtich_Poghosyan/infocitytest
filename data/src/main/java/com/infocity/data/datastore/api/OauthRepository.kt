package com.infocity.data.datastore.api

import com.infocity.entity.Result
import com.infocity.entity.requestmodels.LoginRequest
import com.infocity.entity.responsemodel.user.UserResponse

interface OauthRepository {

    suspend fun login(request: LoginRequest) : Result<UserResponse>
}