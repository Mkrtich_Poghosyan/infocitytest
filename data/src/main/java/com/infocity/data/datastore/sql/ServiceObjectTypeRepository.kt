package com.infocity.data.datastore.sql

import com.infocity.data.datastore.sql.base.BaseRepository
import com.infocity.entity.localmodels.db.ObjectTypeDB
import com.infocity.entity.responsemodel.type.Data

interface ServiceObjectTypeRepository : BaseRepository<ObjectTypeDB> {

    suspend fun getItem(id : Int?) : ObjectTypeDB?

    suspend fun deleteItem(id : Int?)

}