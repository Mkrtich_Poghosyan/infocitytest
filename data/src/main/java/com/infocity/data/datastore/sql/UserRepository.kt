package com.infocity.data.datastore.sql

import com.infocity.data.datastore.sql.base.BaseRepository
import com.infocity.entity.localmodels.db.ObjectTypeDB
import com.infocity.entity.localmodels.db.UserDB

interface UserRepository : BaseRepository<UserDB> {

    suspend fun getItem() : UserDB?

    suspend fun deleteItem(personId : Int?)
}