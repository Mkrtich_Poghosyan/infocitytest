package com.infocity.data.datastore.preference

interface AppSettingRepository {

    companion object{
        const val ACCESS_TOKEN = "access_token"
        const val IS_LOGIN = "isLogin"
        const val IS_DOWNLOADED = "isDownloaded"
    }

    suspend fun getAccessToke() : String?
    suspend fun setAccessToke(value : String?)

    suspend fun isLogin() : Boolean
    suspend fun setLogin(value : Boolean)

    suspend fun isDownloaded() : Boolean
    suspend fun setDownloaded(isDownloaded : Boolean)
}