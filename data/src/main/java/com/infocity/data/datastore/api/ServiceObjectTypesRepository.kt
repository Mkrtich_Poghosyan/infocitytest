package com.infocity.data.datastore.api

import com.infocity.entity.Result
import com.infocity.entity.localmodels.db.ObjectTypeDB
import com.infocity.entity.requestmodels.ObjectTypesRequest

interface ServiceObjectTypesRepository {

    suspend fun getServiceObjectTypes(request : ObjectTypesRequest) : Result<List<ObjectTypeDB>>
}