package com.infocity.data.repository.api

import android.util.Log
import com.infocity.data.dataservice.apiservice.InfoCityApiService
import com.infocity.data.datastore.api.ServiceObjectTypesRepository
import com.infocity.data.datastore.preference.AppSettingRepository
import com.infocity.data.datastore.sql.ServiceObjectTypeRepository
import com.infocity.data.repository.api.base.BaseRepositoryImpl
import com.infocity.data.utils.convertObjectTypeDBList
import com.infocity.data.utils.serializeToMap
import com.infocity.entity.CallException
import com.infocity.entity.Result
import com.infocity.entity.localmodels.db.ObjectTypeDB
import com.infocity.entity.requestmodels.ObjectTypesRequest
import com.infocity.entity.responsemodel.type.ObjectTypes

class ServiceObjectTypesRepositoryImpl(
    private val apiService: InfoCityApiService,
    private val appSetting: AppSettingRepository,
    private val serviceObjectTypeDB : ServiceObjectTypeRepository
) :
    BaseRepositoryImpl(), ServiceObjectTypesRepository {


    override suspend fun getServiceObjectTypes(request : ObjectTypesRequest): Result<List<ObjectTypeDB>> {
        if (!appSetting.isDownloaded()){
            when(val response = apiCall(apiService.getServiceObjectTypes(request.serializeToMap()))){
                is Result.Success -> {
                    Log.wtf("TESTING", "!getServiceObjectTypes Result.Success start save :")
                    response.data?.let {
                        serviceObjectTypeDB.insertAll(it.data.convertObjectTypeDBList())
                        appSetting.setDownloaded(true)
                        return loadDB()
                    }

                }
                is Result.Error -> return Result.Error(CallException(response.errors.errorCode, response.errors.errorMessage))
            }
        }else{
            Log.wtf("TESTING", "!load frome DB : ${!appSetting.isDownloaded()}")
            return loadDB()
        }
       return Result.Error(CallException(404,null))
    }

    private suspend fun loadDB() :  Result<List<ObjectTypeDB>>{
      return  Result.Success(serviceObjectTypeDB.getAll())
    }
}