package com.infocity.data.repository.sql.extensions

@Retention(AnnotationRetention.RUNTIME)
annotation class AutoIncrementPK