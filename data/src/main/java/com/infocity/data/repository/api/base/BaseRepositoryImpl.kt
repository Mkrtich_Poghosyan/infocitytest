package com.infocity.data.repository.api.base

import android.util.Log
import com.infocity.entity.CallException
import retrofit2.Response
import java.lang.Exception
import com.infocity.entity.Result


abstract class BaseRepositoryImpl {

    protected suspend fun <T> getData(
        popularMove: Response<out T>,
    ): Result<T> {
        return  analyzeResponse(popularMove)
    }


    protected suspend fun <T> apiCall(data: Response<out T>) : Result<T> =
        callApi({
            getData(data)
        })

    protected suspend fun <R> callApi(
        call: suspend () -> Result<R>,
        errorMessage: Int = 4567
    ) = try {
        call()
    } catch (e: Exception) {
        Log.i("ErrorMessage", "makeApiCall: ${e.message}")
        Result.Error(CallException<Nothing>(errorMessage,e.message))
    }

    private fun <R> analyzeResponse(response: Response<R>?): Result<R> {
        return when (response?.code()) {
            200 -> {
                if (response.body() != null){
                    val responseBody = response.body()
                    Result.Success(responseBody)
                }else{
                    Result.Success(null)
                }

            }
            else -> {
                Result.Error(CallException<Nothing>(response?.code()?:-1,response?.message()?:"No Error Code"))
            }
        }
    }

}