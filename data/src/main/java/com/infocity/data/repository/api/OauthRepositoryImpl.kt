package com.infocity.data.repository.api

import com.infocity.data.dataservice.apiservice.OauthApiService
import com.infocity.data.datastore.api.OauthRepository
import com.infocity.data.datastore.preference.AppSettingRepository
import com.infocity.data.datastore.sql.UserRepository
import com.infocity.data.repository.api.base.BaseRepositoryImpl
import com.infocity.data.utils.convertRuleDBList
import com.infocity.data.utils.convertUserDb
import com.infocity.entity.Result
import com.infocity.entity.requestmodels.LoginRequest
import com.infocity.entity.responsemodel.user.UserResponse

class OauthRepositoryImpl(
    private val apiService: OauthApiService,
    private val appSetting: AppSettingRepository,
    private val userDB : UserRepository
) : BaseRepositoryImpl(), OauthRepository {

    override suspend fun login(request: LoginRequest): Result<UserResponse> {
        return checkResponse(apiCall(apiService.login(request)))
    }

    private suspend fun checkResponse(response: Result<UserResponse>): Result<UserResponse> {
        return when (response) {
            is Result.Success -> {
                response.data?.let {
                    appSetting.setAccessToke(it.accessToken)
                    appSetting.setLogin(true)
                    userDB.insert(it.convertUserDb())
                }

                response
            }

            is Result.Error -> {
                response
            }
        }
    }
}