package com.infocity.data.repository.sql

import com.infocity.data.datastore.sql.ServiceObjectTypeRepository
import com.infocity.data.repository.sql.extensions.*
import com.infocity.entity.localmodels.db.ObjectTypeDB

class ServiceObjectTypeRepositoryImpl : ServiceObjectTypeRepository {

    override suspend fun getItem(id: Int?): ObjectTypeDB? {
        return queryFirst {
            this.equalTo("ratingId",id)
        }
    }

    override suspend fun deleteItem(id: Int?) {
        delete<ObjectTypeDB> {
            this.equalTo("ratingId",id)
        }
    }

    override suspend fun deleteAll() {
        delete<ObjectTypeDB> {}
    }

    override suspend fun insert(item: ObjectTypeDB) {
        item.save()
    }

    override suspend fun insertAll(itemList: List<ObjectTypeDB>) {
        itemList.saveAll()
    }

    override suspend fun getAll(): List<ObjectTypeDB> {
        return  query { }
    }
}