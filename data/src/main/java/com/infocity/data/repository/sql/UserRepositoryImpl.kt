package com.infocity.data.repository.sql

import com.infocity.data.datastore.sql.UserRepository
import com.infocity.data.repository.sql.extensions.*
import com.infocity.entity.localmodels.db.ObjectTypeDB
import com.infocity.entity.localmodels.db.UserDB

class UserRepositoryImpl : UserRepository {
    override suspend fun getItem(): UserDB? {
        return queryFirst {
        }
    }

    override suspend fun deleteItem(personId: Int?) {
        delete<ObjectTypeDB> {
            this.equalTo("personId",personId)
        }
    }

    override suspend fun deleteAll() {
        delete<ObjectTypeDB> {}
    }

    override suspend fun insert(item: UserDB) {
        item.save()
    }

    override suspend fun insertAll(itemList: List<UserDB>) {
        itemList.saveAll()
    }

    override suspend fun getAll(): List<UserDB> {
        return  query { }
    }
}