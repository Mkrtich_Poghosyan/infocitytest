package com.infocity.data.dataservice.dbservice

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration


abstract class RealmApplication : Application() {

    override fun onCreate() {
        super.onCreate()


        Realm.init(this)
        val migrations = RealmMigrations(this)
        val builder = RealmConfiguration.Builder()
        builder.name("info_city.realm")
        builder.schemaVersion(1)
        //  builder.deleteRealmIfMigrationNeeded();
        //  builder.deleteRealmIfMigrationNeeded();
        builder.migration(migrations)
            .deleteRealmIfMigrationNeeded()
            .schemaVersion(0)
        val config = builder.build()
        Realm.setDefaultConfiguration(config)
    }

}