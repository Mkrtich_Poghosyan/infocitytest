package com.infocity.data.dataservice.preferencesservice

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import com.connectto.data.dataservice.preferenceshelper.base.BaseDataStore
import com.infocity.data.datastore.preference.AppSettingRepository
import com.infocity.data.utils.connectToSetting


class AppSettingPreferences(context: Context) : BaseDataStore(), AppSettingRepository {


    override val datastore: DataStore<Preferences> = context.connectToSetting


    override suspend fun getAccessToke(): String? = getStringByKey(AppSettingRepository.ACCESS_TOKEN)


    override suspend fun setAccessToke(value: String?) {
        setStringByKey(AppSettingRepository.ACCESS_TOKEN, value)
    }

    override suspend fun isLogin(): Boolean = getBooleanByKey(AppSettingRepository.IS_LOGIN)?:false


    override suspend fun setLogin(value: Boolean) {
        setBooleanByKey(AppSettingRepository.IS_LOGIN, value)
    }

    override suspend fun isDownloaded(): Boolean = getBooleanByKey(AppSettingRepository.IS_DOWNLOADED)?:false

    override suspend fun setDownloaded(isDownloaded: Boolean) {
        setBooleanByKey(AppSettingRepository.IS_DOWNLOADED, isDownloaded)
    }
}