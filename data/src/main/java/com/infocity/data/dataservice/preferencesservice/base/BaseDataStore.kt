package com.connectto.data.dataservice.preferenceshelper.base

import android.content.Context
import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.last
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking

abstract class BaseDataStore {

    abstract val datastore : DataStore<Preferences>

    suspend fun getStringByKey(key : String) : String? = getValue(stringPreferencesKey(key))

    suspend fun setStringByKey(key : String, value : String?){
        saveValue(stringPreferencesKey(key), value)
    }

    suspend fun getIntByKey(key : String) : Int? = getValue(intPreferencesKey(key))

    suspend fun setIntByKey(key : String, value : Int?){
        saveValue(intPreferencesKey(key), value)
    }

    suspend fun getBooleanByKey(key : String) : Boolean? = getValue(booleanPreferencesKey(key))

    suspend fun setBooleanByKey(key : String, value : Boolean?){
        saveValue(booleanPreferencesKey(key), value)
    }

    suspend fun getLongByKey(key : String) : Long? = getValue(longPreferencesKey(key))

    suspend fun setLongByKey(key : String, value : Long?){
        saveValue(longPreferencesKey(key), value)
    }

    suspend fun getFloatByKey(key : String) : Float? = getValue(floatPreferencesKey(key))

    suspend fun setFloatByKey(key : String, value : Float?){
        saveValue(floatPreferencesKey(key), value)
    }

    suspend fun getDoubleByKey(key : String) : Double? = getValue(doublePreferencesKey(key))

    suspend fun setDoubleByKey(key : String, value : Double){
        saveValue(doublePreferencesKey(key), value)
    }

    private suspend fun<T> deleteByKey(key : Preferences.Key<T>){
        datastore.edit {
            if (it.contains(key)){
                it.remove(key)
            }
        }
    }

    suspend fun deleteAll(){
        datastore.edit {
            it.clear()
        }
    }

    private suspend fun<T> getValue(key : Preferences.Key<T>) : T?{
        return datastore.data.firstOrNull()?.get(key)
    }

    private suspend fun<T> saveValue(orderKey : Preferences.Key<T>, value : T?) {

        if (value == null){
            deleteByKey(orderKey)
        }else{
            datastore.edit {preference ->
                preference[orderKey] = value
            }
        }
    }
}