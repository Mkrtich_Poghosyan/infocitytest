package com.infocity.data.dataservice.apiservice

import com.infocity.entity.requestmodels.LoginRequest
import com.infocity.entity.responsemodel.user.UserResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface OauthApiService {

    @POST("/api/v1/auth/token")
    suspend fun login(@Body request: LoginRequest): Response<UserResponse>
}