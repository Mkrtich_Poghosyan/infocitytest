package com.infocity.data.dataservice.apiservice

import com.infocity.entity.requestmodels.LoginRequest
import com.infocity.entity.responsemodel.type.ObjectTypes
import com.infocity.entity.responsemodel.user.UserResponse
import retrofit2.Response
import retrofit2.http.*

interface InfoCityApiService {

    @GET("/api/v1/serviceobjecttypes")
    suspend fun getServiceObjectTypes(@QueryMap map : HashMap<String, String>): Response<ObjectTypes>

}