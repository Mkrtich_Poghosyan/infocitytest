package com.infocity.data.header

import com.infocity.data.datastore.preference.AppSettingRepository
import com.infocity.data.utils.AUTHENTICATE_KEY
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class InfoCityHeaderInterceptor(private val appSetting: AppSettingRepository) : Interceptor {
    lateinit var request: Request
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val requestBuilder = runBlocking {
            originalRequest.newBuilder()
                .method(originalRequest.method, originalRequest.body)
                .header("Content-Type", "application/json")
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", "$AUTHENTICATE_KEY ${appSetting.getAccessToke()}")
        }
        request=  requestBuilder.build()
        return chain.proceed(request)
    }
}