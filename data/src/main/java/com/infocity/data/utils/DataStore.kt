package com.infocity.data.utils

import android.content.Context
import androidx.datastore.preferences.preferencesDataStore

val Context.connectToSetting by  preferencesDataStore("info_city-app-settings.json")