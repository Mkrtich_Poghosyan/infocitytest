package com.infocity.data.utils

import com.infocity.entity.localmodels.db.ObjectTypeDB
import com.infocity.entity.localmodels.db.RuleDB
import com.infocity.entity.localmodels.db.UserDB
import com.infocity.entity.responsemodel.type.Data
import com.infocity.entity.responsemodel.type.ObjectTypes
import com.infocity.entity.responsemodel.user.Rule
import com.infocity.entity.responsemodel.user.UserResponse
import io.realm.RealmList

fun UserResponse.convertUserDb() : UserDB{
    val userDB = UserDB()

    userDB.personId = personId
    userDB.accessToken = accessToken
    userDB.companyId = companyId
    userDB.email = email
    userDB.login = login
    userDB.email = email
    userDB.memberId = memberId
    userDB.name = name
    userDB.positionId = positionId
    userDB.roleId = roleId
    userDB.rules = rules.convertRuleDBList()
    return userDB
}


fun List<Rule>.convertRuleDBList() : RealmList<RuleDB>{
    val list = RealmList<RuleDB>()
    forEach {
        list.add(it.convertRuleDB())
    }
    return list
}

fun Rule.convertRuleDB() : RuleDB {

    val ruleDB = RuleDB()
    ruleDB.numberId = numberId
    ruleDB.canCreate = canCreate
    ruleDB.canDelete = canDelete
    ruleDB.canEdit = canEdit
    ruleDB.canRead = canRead
    ruleDB.isCrudOperationRule = isCrudOperationRule
    ruleDB.memberId = memberId
    ruleDB.ruleCode = ruleCode
    ruleDB.ruleId = ruleId
    return ruleDB
}

fun List<Data>.convertObjectTypeDBList() : List<ObjectTypeDB>{
    val dbList = arrayListOf<ObjectTypeDB>()
    forEach {
        dbList.add(it.convertObjectTypeDB())
    }
    return dbList
}

fun List<Data>.convertObjectTypeDBRealmList() : RealmList<ObjectTypeDB>{
    val dbList = RealmList<ObjectTypeDB>()
    forEach {
        dbList.add(it.convertObjectTypeDB())
    }
    return dbList
}

fun Data.convertObjectTypeDB() : ObjectTypeDB{
    val objectTypeDB = ObjectTypeDB()

    objectTypeDB.id = id
    objectTypeDB.number = number
    objectTypeDB.parentId = parentId
    objectTypeDB.parent = parent?.convertObjectTypeDB()
    objectTypeDB.layerName = layerName
    objectTypeDB.layerSystemName = layerSystemName
    objectTypeDB.geomType = geomType
    objectTypeDB.children = children?.convertObjectTypeDBRealmList()
    objectTypeDB.hasChildren = hasChildren
    objectTypeDB.workTypes = workTypes
    objectTypeDB.serviceAttributes = serviceAttributes
    objectTypeDB.companies = companies
    objectTypeDB.members = members
    objectTypeDB.serviceObjects = serviceObjects
    objectTypeDB.name = name
    objectTypeDB.createdBy = createdBy
    objectTypeDB.createdAt = createdAt
    objectTypeDB.updatedAt = updatedAt
    objectTypeDB.updatedBy = updatedBy
    objectTypeDB.deletedAt = deletedAt
    objectTypeDB.deletedBy = deletedBy
    objectTypeDB.isActive = isActive

    return objectTypeDB
}




