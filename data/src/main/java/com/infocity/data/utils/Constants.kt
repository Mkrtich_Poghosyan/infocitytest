package com.infocity.data.utils

const val CONNECTION_TIMEOUTS_MIN = 60L
const val BASE_URL = "https://test.infocity.me/"
const val AUTHENTICATE_KEY = "Bearer "