package com.infocity.data.utils

import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.google.gson.reflect.TypeToken
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi


fun Int.expiredTime(): Long {
    return System.currentTimeMillis() + this * 1000
}

fun getCurrentTime(): Long {
    return System.currentTimeMillis() - 5000
}

val gson = Gson()

fun <T> T.serializeToMap(): HashMap<String, String> {
    return convert()
}

inline fun <I, reified O> I.convert(): O {
    val json = gson.toJson(this)
    return gson.fromJson(json, object : TypeToken<O>() {}.type)
}



@Throws(JsonParseException::class)
inline fun <reified T : Any> String?.jsonStringToDataClass() : T?{

    val moshi = Moshi.Builder().build()
    val adapter : JsonAdapter<T> = moshi.adapter(T::class.java)

    return try {
        if (!isNullOrBlank())
          adapter.fromJson(this)
        else null
    }catch (e : JsonParseException){
        e.printStackTrace()
        null
    }
}









