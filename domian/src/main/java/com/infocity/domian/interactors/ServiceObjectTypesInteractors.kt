package com.infocity.domian.interactors

import com.infocity.entity.Result
import com.infocity.entity.localmodels.db.ObjectTypeDB
import com.infocity.entity.requestmodels.ObjectTypesRequest

interface ServiceObjectTypesInteractors {

    suspend fun getServiceObjectTypes(request : ObjectTypesRequest) : Result<List<ObjectTypeDB>>
}