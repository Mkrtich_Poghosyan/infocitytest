package com.infocity.domian.di

import com.infocity.domian.interactors.ServiceObjectTypesInteractors
import com.infocity.domian.usecase.ServiceObjectTypesUseCase
import org.koin.dsl.module

val interactorModule = module {
    single<ServiceObjectTypesInteractors> { ServiceObjectTypesUseCase(get()) }
}