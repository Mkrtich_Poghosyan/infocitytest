package com.infocity.domian.usecase

import android.util.Log
import com.infocity.data.datastore.api.ServiceObjectTypesRepository
import com.infocity.domian.interactors.ServiceObjectTypesInteractors
import com.infocity.entity.requestmodels.ObjectTypesRequest
import com.infocity.entity.Result
import com.infocity.entity.localmodels.db.ObjectTypeDB

class ServiceObjectTypesUseCase(private val objectTypesRepository: ServiceObjectTypesRepository) : ServiceObjectTypesInteractors{


    override suspend fun getServiceObjectTypes(request : ObjectTypesRequest): Result<List<ObjectTypeDB>> {

        return objectTypesRepository.getServiceObjectTypes(request)
    }

}