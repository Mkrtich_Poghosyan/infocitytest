package com.infocity.entity.localmodels.db

import com.infocity.entity.responsemodel.user.Rule
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.RealmResults
import io.realm.annotations.LinkingObjects
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import io.realm.annotations.RealmField

@RealmClass(name = UserDB.tableName)
open class UserDB : RealmObject(){

    @PrimaryKey
    @RealmField(name = "personId")
    var personId: String? = null

    var accessToken: String? = null
    var companyId: String? = null
    var email: String? = null
    var login: String? = null
    var memberId: String? = null
    var name: String? = null

    var positionId: String? = null
    var roleId: String? = null

    var rules: RealmList<RuleDB>? = null

    companion object {
        const val tableName = "UserDB"
    }
}