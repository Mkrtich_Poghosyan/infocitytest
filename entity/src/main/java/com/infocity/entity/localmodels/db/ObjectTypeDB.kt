package com.infocity.entity.localmodels.db

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import io.realm.annotations.RealmField

@RealmClass(name = ObjectTypeDB.tableName)
open class ObjectTypeDB : RealmObject(){
    @PrimaryKey
    @RealmField(name = "object_type_id")
    var id: String? = "empty"
    var number: Int? = null
    var parentId: String? = null
    var parent: ObjectTypeDB?= null
    var layerName: String? = null
    var layerSystemName: String?= null
    var geomType: String?= null
    var children: RealmList<ObjectTypeDB>?= null
    var hasChildren: Boolean = false
    var workTypes: String?= null
    var serviceAttributes: String?= null
    var companies: String?= null
    var members: String?= null
    var serviceObjects: String?= null
    var name: String?= null
    var createdBy: String?= null
    var createdAt: String?= null
    var updatedAt: String?= null
    var updatedBy: String?= null
    var deletedAt: String?= null
    var deletedBy: String?= null
    var isActive: Boolean = true


    companion object {
        const val tableName = "ObjectTypeDB"
    }
}
