package com.infocity.entity.localmodels.db

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import io.realm.annotations.RealmField

@RealmClass(name = RuleDB.tableName)
open class RuleDB : RealmObject(){
    @PrimaryKey
    @RealmField(name = "numberId")
    var numberId: Int = -1
    var canCreate: Boolean = false
    var canDelete: Boolean = false
    var canEdit: Boolean = false
    var canRead: Boolean = false
    var isCrudOperationRule: Boolean = false
    var memberId: String? = null

    var ruleCode: String? = null
    var ruleId: String? = null


    companion object {
        const val tableName = "RuleDBObj"
    }
}