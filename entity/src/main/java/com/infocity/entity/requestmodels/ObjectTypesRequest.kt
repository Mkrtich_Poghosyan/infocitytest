package com.infocity.entity.requestmodels

data class ObjectTypesRequest(val Skip : Int, val Take : Int)
