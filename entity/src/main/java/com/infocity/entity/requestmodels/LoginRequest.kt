package com.infocity.entity.requestmodels

data class LoginRequest(val login: String, val password: String, val returnUrl : String = "http://test.infocity.me/swagger")