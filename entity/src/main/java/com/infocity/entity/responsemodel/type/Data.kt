package com.infocity.entity.responsemodel.type

data class Data(

    val number: Int,
    val parentId: String?,
    val parent: Data?,
    val layerName: String,
    val layerSystemName: String,
    val geomType: String,
    val children: List<Data>?,
    val hasChildren: Boolean,
    val workTypes: String?,
    val serviceAttributes: String?,
    val companies: String?,
    val members: String?,
    val serviceObjects: String?,
    val id: String,
    val name: String,
    val createdBy: String,
    val createdAt: String,
    val updatedAt: String,
    val updatedBy: String,
    val deletedAt: String?,
    val deletedBy: String?,
    val isActive: Boolean










)