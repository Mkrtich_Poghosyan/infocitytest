package com.infocity.app.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.infocity.app.R
import com.infocity.app.databinding.ActivityMainBinding
import com.infocity.app.ui.adapter.ExpandableAdapter
import com.infocity.app.ui.viewmodel.LoginViewModel
import com.infocity.app.utils.isNetworkAvailable
import com.infocity.app.utils.viewBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import okhttp3.Dispatcher
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val viewModel: LoginViewModel by viewModel()
    private val viewBinding: ActivityMainBinding by viewBinding()
    private val coroutineScope = CoroutineScope(Dispatchers.Main)
    private val adapter = ExpandableAdapter()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(viewBinding.root)

        viewBinding.recyclerView.setHasFixedSize(true)
        viewBinding.recyclerView.layoutManager = LinearLayoutManager(this)
        viewBinding.recyclerView.adapter = adapter

        launchSharedFlow(viewModel.list){list ->
            list?.let {
                adapter.setNewList(it)
            }
        }

        launchSharedFlow(viewModel.loading){
            viewBinding.progressBar.visibility = if(it) View.VISIBLE else View.GONE
        }


        if (isNetworkAvailable(this)){
            viewModel.start()
        }else{
            val snack = Snackbar.make(viewBinding.root,getString(R.string.no_internet_connection),Snackbar.LENGTH_LONG)
            snack.show()
        }




    }

    private fun <T>launchSharedFlow(sharedFlow : StateFlow<T>, action: suspend (T) -> Unit){
        coroutineScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED){
                sharedFlow.collectLatest(action)
            }

        }
    }
}