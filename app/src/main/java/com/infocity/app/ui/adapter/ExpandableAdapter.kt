package com.infocity.app.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.infocity.app.databinding.ViewHolderBinding
import com.infocity.app.ui.viewholder.ExpandableViewHolder
import com.infocity.entity.localmodels.db.ObjectTypeDB

class ExpandableAdapter : RecyclerView.Adapter<ExpandableViewHolder>(){

    private var list: List<ObjectTypeDB>? = null
    private var expandableList  = arrayListOf<Boolean>()


    private fun initExpandableList(size : Int){
        for (i in 0 until size){
            expandableList.add(false)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setNewList(list: List<ObjectTypeDB>) {
        initExpandableList(list.size)
        this.list = list

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExpandableViewHolder {
       return ExpandableViewHolder(ViewHolderBinding.inflate(LayoutInflater.from(parent.context)),::onClickItem)
    }

    override fun onBindViewHolder(holder: ExpandableViewHolder, position: Int) {
        list?.get(position)?.let {
            holder.onBind(it, expandableList[position])
        }
    }

    override fun getItemCount(): Int = list?.size?:0


    private fun onClickItem(position : Int){
        expandableList[position] = !expandableList[position]
        notifyItemChanged(position)
    }

}