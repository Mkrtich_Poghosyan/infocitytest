package com.infocity.app.ui.viewmodel

import android.app.Application
import android.util.Log
import com.infocity.app.base.BaseViewModel
import com.infocity.data.datastore.api.OauthRepository
import com.infocity.data.datastore.api.ServiceObjectTypesRepository
import com.infocity.data.datastore.preference.AppSettingRepository
import com.infocity.domian.interactors.ServiceObjectTypesInteractors
import com.infocity.entity.Result
import com.infocity.entity.localmodels.db.ObjectTypeDB
import com.infocity.entity.requestmodels.LoginRequest
import com.infocity.entity.requestmodels.ObjectTypesRequest
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow

class LoginViewModel(
    private val oauthRepository: OauthRepository,
    private val objectTypesRepository: ServiceObjectTypesInteractors,
    private val appSetting: AppSettingRepository,
    app: Application
) : BaseViewModel(app) {

    private val mList = MutableStateFlow<List<ObjectTypeDB>?>(null)
    var list = mList.asStateFlow()

    fun start() {

        launch {
            mLoading.value = true
            if (!appSetting.isLogin()){
                login()
            }else{
                getData()
            }
        }
    }

    private fun login() {
        launch {

            val result = oauthRepository.login(LoginRequest("test", "QQQqqq@2"))
            when (result) {
                is Result.Success -> {
                    getData()
                }

                is Result.Error -> {
                    mLoading.value = false
                }
            }
        }
    }

    private fun getData() {
        launch {
            val resultObject =
                objectTypesRepository.getServiceObjectTypes(ObjectTypesRequest(0, 100))

            when (resultObject) {
                is Result.Success -> {

                    val list = resultObject.data
                    Log.wtf("TESTING", "!load getData() : ${list?.size}")
                    mLoading.value = false

                    mList.value = list
                }
                is Result.Error -> {
                    mLoading.value = false
                }
            }
        }
    }

}