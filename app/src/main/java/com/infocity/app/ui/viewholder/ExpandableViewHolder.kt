package com.infocity.app.ui.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.infocity.app.R
import com.infocity.app.databinding.ViewHolderBinding
import com.infocity.app.ui.adapter.ExpandableAdapter
import com.infocity.entity.localmodels.db.ObjectTypeDB


class ExpandableViewHolder(private val binding: ViewHolderBinding, onClick : (Int) -> Unit) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.root.setOnClickListener {
            onClick.invoke(adapterPosition)
        }
    }

    fun onBind(obj : ObjectTypeDB, isExpand : Boolean){

        binding.textView.text = obj.name

        if (obj.children != null && obj.children!!.size > 0){
            showOrHideExpandIcon(true, isExpand)
            if (isExpand){
                binding.expandIcon.visibility = View.VISIBLE
                val adapter = ExpandableAdapter()
                binding.itemRecyclerView.visibility = View.VISIBLE
                binding.itemRecyclerView.adapter = adapter
                adapter.setNewList(obj.children!!)
            }
        }else{
            showOrHideExpandIcon(false, isExpand)
            binding.itemRecyclerView.visibility = View.GONE
        }
    }

    private fun showOrHideExpandIcon(isShow : Boolean, isExpand: Boolean){
        if (isShow){
            binding.expandIcon.visibility = View.VISIBLE
             if (isExpand){
                 binding.expandIcon.setImageResource(R.drawable.ic_expand_more_black_24dp)
             }else{
                 binding.expandIcon.setImageResource(R.drawable.ic_expand_less_black_24dp)
             }
        }else{
            binding.expandIcon.visibility = View.INVISIBLE
        }
    }

}
