package com.infocity.app

import android.app.Application
import com.infocity.app.di.viewModelModule
import com.infocity.data.dataservice.dbservice.RealmApplication
import com.infocity.data.di.apiModule
import com.infocity.data.di.databaseModule
import com.infocity.data.di.preferencesModule
import com.infocity.data.di.repositoryModule
import com.infocity.domian.di.interactorModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class InfoCityApp : RealmApplication() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@InfoCityApp)
            koin.loadModules(modules)
        }
    }

    private val modules = listOf(
        preferencesModule,
        apiModule,
        databaseModule,
        repositoryModule,
        interactorModule,
        viewModelModule
    )

}