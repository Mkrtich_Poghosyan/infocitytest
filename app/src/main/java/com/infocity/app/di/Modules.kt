package com.infocity.app.di

import com.infocity.app.ui.viewmodel.LoginViewModel
import org.koin.dsl.module

val viewModelModule = module {

    single { LoginViewModel(get(), get(), get(), get()) }
}