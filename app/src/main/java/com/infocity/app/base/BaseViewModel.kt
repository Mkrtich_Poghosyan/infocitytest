package com.infocity.app.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

abstract class BaseViewModel(app: Application) : AndroidViewModel(app){


    protected val mLoading = MutableStateFlow<Boolean>(false)
    var loading = mLoading.asStateFlow()

  //  protected val mNetworkStat = MutableSharedFlow<NetworkStat>()
  //  val networkStat = mNetworkStat.asSharedFlow()

    val mHasNetwork = MutableSharedFlow<Boolean>()
    val hasNetwork = mHasNetwork.asSharedFlow()

    fun  launch(function: suspend  () -> Unit) = viewModelScope.run {
        viewModelScope.launch(Dispatchers.IO) {
            function.invoke()
        }
    }

    protected fun changeSharedFlow(function: suspend () -> Unit) = viewModelScope.launch {
        function.invoke()
    }

    fun changeLoad(isLoaded : Boolean){
        changeSharedFlow {
            mLoading.emit(isLoaded)
        }
    }

//    fun setNetworkStat(stat: NetworkStat){
//        changeSharedFlow{
//            mNetworkStat.emit(stat)
//        }
//    }

    fun changeNetwork(isNetwork : Boolean){
        changeSharedFlow {
            mHasNetwork.emit(isNetwork)
        }
    }

}